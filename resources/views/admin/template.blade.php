<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset("assets/favicon.ico") }}">

    <title>Projeto Notícias Escape</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset("assets/libs/bootstrap-4.0.0/css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="{{ asset("assets/libs/font-awesome-4.7.0/css/font-awesome.min.css") }}">
    <!-- Modal -->
    <link href="{{ asset("assets/libs/fancybox/dist/jquery.fancybox.min.css") }}" type="text/css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ asset("assets/css/starter-template.css") }}" rel="stylesheet">
    <!-- summernote -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="{{ URL::to("") }}">CRUD</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ URL::route("admin.noticia.categoria") }}">Categorias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ URL::route("admin.noticia") }}">Notícias</a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container">

    @section("content")
        
    @show

    </main><!-- /.container -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{{ asset("assets/js/popper.min.js") }}"></script>
    <script src="{{ asset("assets/js/escape.js") }}"></script>
    <script src="{{ asset("assets/libs/bootstrap-4.0.0/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("assets/libs/fancybox/dist/jquery.fancybox.min.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.js"></script>
  </body>
</html>
