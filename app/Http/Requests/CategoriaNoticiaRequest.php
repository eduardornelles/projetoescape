<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaNoticiaRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required|max:255'
        ];
    }

    public function messages(){

        return [
            'titulo.required' => 'O título é obrigatório',
            'titulo.max' => 'O título não pode exceder 255 caracteres'
        ];
    }
}
