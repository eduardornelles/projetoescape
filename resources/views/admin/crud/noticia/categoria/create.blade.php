@extends("admin.template")
@section("content")
    <h3>{{ $pageTitle }}</h3>
    @if( $registro->id != null )    
        {!! Form::open([ 'url' => URL::route('admin.noticia.categoria.update', $registro->id ), 'method' => 'POST', 'files' => true,  'class' => 'form_validate form-horizontal'  ]) !!}
    @else
        {!! Form::open([ 'url' => URL::route('admin.noticia.categoria.store' ), 'method' => 'POST', 'files' => true,  'class' => 'form_validate form-horizontal'  ]) !!}
    @endif
    @include('admin.crud.noticia.categoria._form')
    <div class="form-group padding-vertical">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="submit" class="btn btn-success">Salvar</button>
            <a href="{{ URL::route('admin.noticia.categoria') }}" id="btn_cancelar" class="btn btn-default">Cancelar</a>
        </div>
    </div>
    </div>
    {!! Form::close() !!}
@stop
    