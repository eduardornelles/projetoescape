@extends("admin.template")
@section("content")
    <div class="row">
        <div class="col-md-6">
            <h3>{{ $pageTitle }}</h3>
        </div>
        <div class="col-md-6">
            <a href="{{ URL::route('admin.noticia.create' ) }}" class="btn btn-success pull-right" type="button">
                Adicionar registro
            </a>
        </div>
    </div>
    @if( isset( $registros ) )
        <div class="table-responsive">
            <table class="table-hover table table-striped">
                <thead>
                <tr>
                    <th class="table-id">#</th>
                    <th>Título</th>
                    <th>Categoria</th>
                    <th class="table-actions text-center">Ações</th>
                </tr>
                </thead>
                <tbody>
                     @foreach( $registros as $row)
                    <tr>
                        <td class="table-id">{{ $row->id }}</td>
                        <td>{{ $row->titulo }}</td>
                        <td>{{ $row->categoria->titulo }}</td>
                        <td class="table-actions text-center">
                            <a data-toggle="tooltip" title="Editar Registro" class='btn btn-success btn-sm ' href='{{ URL::route('admin.noticia.edit' ,$row->id ) }}'><i class='fa fa-edit'></i></a>
                            <a data-toggle="tooltip" title="Excluir Registro" class='btn btn-danger btn-sm' href='{{ URL::route('admin.noticia.delete' ,$row->id ) }}'><i class='fa fa-trash'></i></a>
                            <a data-toggle="tooltip" title="Ver Registro" class='btn btn-primary btn-sm' data-fancybox data-type="ajax" data-src="{{ URL::route('admin.noticia.view' ,$row->id ) }}"   href="javascript:;"><i class='fa fa-eye'></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if(count($registros) == 0)
            Nenhum dado encontrado.
        @else
            {!! $registros->render() !!}
        @endif
    @endif
@stop
