<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoticiaRequest extends FormRequest
{
     public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required|max:255',
            'texto' => 'required',
            'titulo' => 'required|max:255',
            'noticia_categoria_id' => 'required|exists:noticia_categoria,id',
            'imagem' => 'file|mimes:png,jpeg,jpg,gif'
        ];
    }

    public function messages(){

        return [
            'titulo.required' => 'O título é obrigatório',
            'titulo.max' => 'O título não pode exceder 255 caracteres',
            'texto.required' => 'O texto é obrigatório',
            'noticia_categoria_id.required' => 'A categoria é obrigatória',
            'noticia_categoria_id.exists' => 'Selecione uma categoria existente',
            'imagem.file' => 'Selecione uma imagem com extensão PNG, JPG ou GIF',
            'imagem.mimes' => 'Selecione uma imagem com extensão PNG, JPG ou GIF',
        ];
    }
}
