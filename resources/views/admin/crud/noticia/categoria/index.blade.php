@extends("admin.template")
@section("content")
    <div class="row">
        <div class="col-md-6">
            <h3>{{ $pageTitle }}</h3>
        </div>
        <div class="col-md-6">
            <a href="{{ URL::route('admin.noticia.categoria.create' ) }}" class="btn btn-success pull-right" type="button">
                Adicionar registro
            </a>
        </div>
    </div>
    @if( isset( $registros ) )
        <div class="table-responsive">
            <table class="table-hover table table-striped">
                <thead>
                <tr>
                    <th class="table-id">#</th>
                    <th>Título</th>
                    <th class="table-actions text-center">Ações</th>
                </tr>
                </thead>
                <tbody>
                     @foreach( $registros as $row)
                    <tr>
                        <td class="table-id">{{ $row->id }}</td>
                        <td>{{ $row->titulo }}</td>
                        <td class="table-actions text-center">
                            <a data-toggle="tooltip" title="Editar Registro" class='btn btn-success btn-sm ' href='{{ URL::route('admin.noticia.categoria.edit' ,$row->id ) }}'><i class='fa fa-edit'></i></a>
                            <a data-toggle="tooltip" title="Excluir Registro" class='btn btn-danger btn-delete btn-sm' href='{{ URL::route('admin.noticia.categoria.delete' ,$row->id ) }}'><i class='fa fa-trash'></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if(count($registros) == 0)
            Nenhum dado encontrado.
        @else
            {!! $registros->render() !!}
        @endif
    @endif
@stop
