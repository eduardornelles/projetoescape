$(document).ready( function(){
    $("[data-fancybox]").fancybox({
	smallBtn: true
    });
    
    $(".disabled").click( function(){
       return false;
    });

    $(".btn-delete").click( function(){
        if( confirm("Tem certeza que deseja deletar este registro?") ){
            return true;
        }
        return false;
    });
    
    if( $('.summernote').length > 0 ) {
        $('.summernote').summernote({
            height: 250
        });
    }

    $('[data-toggle="tooltip"]').tooltip( {html:true} );
});
