<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Noticia;
use Illuminate\Support\Facades\Config;
use \App\Http\Requests\NoticiaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \Illuminate\Support\Facades\Session;

class NoticiaController extends Controller
{
    public function index() {
        $registros = Noticia::orderBy('id','desc')->paginate( Config::get("app.pagination") );
        $registros->setPath('noticia');

        $data = [
            'pageTitle' => "Gerenciar Notícias",
            'registros'     => $registros
        ];

        return view('admin.crud.noticia.index', $data );
    }

    public function create()
    {
        $registro = new Noticia();
        $categorias = Noticia\Categoria::get();
        
        $data = [
            'pageTitle' => "Adicionar Notícia",
            'registro' => $registro,
            'categorias' => $categorias
        ];
        return view("admin.crud.noticia.create", $data);
    }

    public function store(NoticiaRequest $request)
    {
        $registro = Noticia::create( Input::all() );
        
        if( $logoUploadExt = $request->file('imagem') != null ){
            $logoUploadExt = $request->file('imagem')->getClientOriginalExtension();
            $logoUpload = md5($request->file('imagem')->getClientOriginalName().rand(0,999)).".".$logoUploadExt;
            $request->file('imagem')->move(
                base_path() . '/public/uploads/'. date("Y/m/"), $logoUpload
            );

            $logo = date("Y/m/").$logoUpload;
            $registro->imagem = $logo;
            $registro->update();
        }

        Session::flash('alert-success', 'Registro cadastrado com sucesso!!');

        return Redirect::route("admin.noticia");
    }

    public function edit($id)
    {
        $registro = Noticia::find( $id );
        $categorias = Noticia\Categoria::get();

        $data = [
            'pageTitle' => "Editar Notícia",
            'registro' => $registro,
            'categorias' => $categorias
        ];

        return view("admin.crud.noticia.create", $data);
    }

    public function update(NoticiaRequest $request, $id){

        $registro = Noticia::find( $id );
        $registro->fill( Input::all() )->update();
        
        if( $logoUploadExt = $request->file('imagem') != null ){
            $logoUploadExt = $request->file('imagem')->getClientOriginalExtension();
            $logoUpload = md5($request->file('imagem')->getClientOriginalName().rand(0,999)).".".$logoUploadExt;
            $request->file('imagem')->move(
                base_path() . '/public/uploads/'. date("Y/m/"), $logoUpload
            );

            $logo = date("Y/m/").$logoUpload;
            $registro->imagem = $logo;
            $registro->update();
        }
        
        Session::flash('alert-success', 'Registro alterado com sucesso!!');

        return Redirect::route("admin.noticia");

    }

    
    public function destroy($id)
    {
        Noticia::destroy( $id );
        return Redirect::back();
    }
    
    public function view($id)
    {
        $registro = Noticia::find( $id );

        $data = [
            'registro' => $registro,
        ];

        return view("admin.crud.noticia.view", $data);
    }
}
