<article>
    <h1>{{ $registro->titulo }}</h1>
    <img src="{{ asset("uploads/".$registro->imagem) }}" class="img-responsive responsive" style="max-width: 100%" />
    {!! $registro->texto !!}
</article>