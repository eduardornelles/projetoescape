<?php

namespace App\Models\Noticia;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'noticia_categoria';
    public $fillable = ['titulo'];
    
    public function noticias(){
        return $this->hasMany("App\Models\Noticia","noticia_categoria_id","id");
    }
}
