<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Noticia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('noticia_categoria_id')->unsigned();
            $table->string('titulo');
            $table->text('texto');
            $table->string('imagem',50)->nullable();
            $table->timestamps();
            
             $table->foreign('noticia_categoria_id')->references('id')->on('noticia_categoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('noticia');
    }
}
