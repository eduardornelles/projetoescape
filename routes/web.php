<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Admin\DashboardController@index');

Route::group(['prefix' => 'admin' ] , function() {
    
    Route::group(['prefix' => 'crud' ] , function() {
        
        Route::group(['prefix' => 'noticia' ] , function() {
            
            Route::get('/', [ 'as' => 'admin.noticia', 'uses' => 'Admin\NoticiaController@index' ] );
            Route::get('create', [ 'as' => 'admin.noticia.create', 'uses' => 'Admin\NoticiaController@create' ] );
            Route::get('edit/{id}', [ 'as' => 'admin.noticia.edit', 'uses' => 'Admin\NoticiaController@edit' ], function( $id ){ } );
            Route::get('delete/{id}', [ 'as' => 'admin.noticia.delete', 'uses' => 'Admin\NoticiaController@destroy' ], function( $id ){ } );
            Route::post('store', [ 'as' => 'admin.noticia.store', 'uses' => 'Admin\NoticiaController@store' ] );
            Route::post('update/{id}', [ 'as' => 'admin.noticia.update', 'uses' => 'Admin\NoticiaController@update' ], function( $id ){ } );
            Route::get('view/{id}', [ 'as' => 'admin.noticia.view', 'uses' => 'Admin\NoticiaController@view' ], function( $id ){ } );
            
            Route::group(['prefix' => 'categoria' ] , function() {
                Route::get('/', [ 'as' => 'admin.noticia.categoria', 'uses' => 'Admin\CategoriaNoticiaController@index' ] );
		Route::get('create', [ 'as' => 'admin.noticia.categoria.create', 'uses' => 'Admin\CategoriaNoticiaController@create' ] );
		Route::get('edit/{id}', [ 'as' => 'admin.noticia.categoria.edit', 'uses' => 'Admin\CategoriaNoticiaController@edit' ], function( $id ){ } );
		Route::get('delete/{id}', [ 'as' => 'admin.noticia.categoria.delete', 'uses' => 'Admin\CategoriaNoticiaController@destroy' ], function( $id ){ } );
		Route::post('store', [ 'as' => 'admin.noticia.categoria.store', 'uses' => 'Admin\CategoriaNoticiaController@store' ] );
		Route::post('update/{id}', [ 'as' => 'admin.noticia.categoria.update', 'uses' => 'Admin\CategoriaNoticiaController@update' ], function( $id ){ } );
            });
        
        });
    });

    
});