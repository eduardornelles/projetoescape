<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Noticia\Categoria;
use Illuminate\Support\Facades\Config;
use \App\Http\Requests\CategoriaNoticiaRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \Illuminate\Support\Facades\Session;

class CategoriaNoticiaController extends Controller
{
    public function index() {
        $registros = Categoria::orderBy('id','desc')->paginate( Config::get("app.pagination") );
        $registros->setPath('noticia_categoria');

        $data = [
            'pageTitle' => "Gerenciar Categorias",
            'registros'     => $registros
        ];

        return view('admin.crud.noticia.categoria.index', $data );
    }

    public function create()
    {
        $registro = new Categoria();

        $data = [
            'pageTitle' => "Adicionar Categoria",
            'registro' => $registro
        ];
        return view("admin.crud.noticia.categoria.create", $data);
    }

    public function store(CategoriaNoticiaRequest $request)
    {
        $registro = Categoria::create( Input::all() );
        Session::flash('alert-success', 'Registro cadastrado com sucesso!!');

        return Redirect::route("admin.noticia.categoria");
    }

    public function edit($id)
    {
        $registro = Categoria::find( $id );

        $data = [
            'pageTitle' => "Editar Categoria",
            'registro' => $registro
        ];

        return view("admin.crud.noticia.categoria.create", $data);
    }

    public function update(CategoriaNoticiaRequest $request, $id){

        $registro = Categoria::find( $id );
        $registro->fill( Input::all() )->update();
        
        Session::flash('alert-success', 'Registro alterado com sucesso!!');

        return Redirect::route("admin.noticia.categoria");

    }
    
    public function destroy($id)
    {
        Categoria::destroy( $id );
        return Redirect::back();
    }
}
