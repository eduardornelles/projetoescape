<div class="form-group">
    {!! Form::label('titulo','Título:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('titulo', $registro->titulo , [ 'class' => 'form-control', 'required' ] ) !!}
    </div>
</div>
