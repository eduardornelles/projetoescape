<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $table = 'noticia';
    public $fillable = ['titulo','noticia_categoria_id','texto','imagem'];
    
    public function categoria(){
        return $this->belongsTo("App\Models\Noticia\Categoria","noticia_categoria_id","id");
    }
}
