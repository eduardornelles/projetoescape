-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.14 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.4.0.5130
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela escape.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela escape.migrations: 4 rows
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2017_12_18_203433_CategoriaNoticia', 1),
	(4, '2017_12_18_203444_Noticia', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Copiando estrutura para tabela escape.noticia
CREATE TABLE IF NOT EXISTS `noticia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noticia_categoria_id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `noticia_noticia_categoria_id_foreign` (`noticia_categoria_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela escape.noticia: 0 rows
DELETE FROM `noticia`;
/*!40000 ALTER TABLE `noticia` DISABLE KEYS */;
INSERT INTO `noticia` (`id`, `noticia_categoria_id`, `titulo`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
	(2, 5, 'Teste', 'asdasdasd', '2017/12/40929207e1813117bbcaa5b9155190fe.jpg', '2017-12-18 21:51:40', '2017-12-18 21:51:40'),
	(3, 6, 'Teste', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fermentum urna justo, at hendrerit eros fringilla sed. Aenean maximus ultricies ipsum, sit amet mattis nisl viverra nec. Vestibulum eget mi diam. Pellentesque molestie ex posuere mi dictum molestie. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse egestas, mi a tempus volutpat, velit elit auctor urna, sit amet rutrum tortor sapien in ipsum. Donec tristique leo in nibh posuere, sit amet scelerisque risus lacinia. Phasellus condimentum augue elit, nec elementum urna volutpat scelerisque. Nulla non leo nulla. Sed elit eros, feugiat sed dignissim non, consectetur vel purus. Praesent sapien arcu, semper vel fringilla vel, dapibus eu quam. Ut turpis urna, semper sed suscipit vitae, rutrum vel erat. Fusce neque felis, consectetur eget leo eget, sollicitudin fringilla nunc. Mauris sodales ligula sit amet odio ullamcorper, in cursus velit iaculis. Vestibulum a lorem non mi dignissim fringilla. Donec auctor eget enim id sodales.\r\n\r\nEtiam varius porta enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac ultrices lectus, et mollis felis. Duis at mauris ut sem rutrum lobortis non in tellus. Suspendisse varius bibendum velit vel imperdiet. In luctus lectus justo, non cursus velit porta quis. Etiam iaculis odio et nisl bibendum, ut scelerisque tortor scelerisque. Vestibulum sagittis at nibh non porttitor. Aenean ultrices metus eu scelerisque bibendum. Morbi facilisis lectus ac libero mattis, quis rhoncus felis facilisis.', '2017/12/d195bd213f4fcd17110e2f9a1ebfde00.jpg', '2017-12-18 21:51:55', '2017-12-18 22:21:56');
/*!40000 ALTER TABLE `noticia` ENABLE KEYS */;

-- Copiando estrutura para tabela escape.noticia_categoria
CREATE TABLE IF NOT EXISTS `noticia_categoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela escape.noticia_categoria: 2 rows
DELETE FROM `noticia_categoria`;
/*!40000 ALTER TABLE `noticia_categoria` DISABLE KEYS */;
INSERT INTO `noticia_categoria` (`id`, `titulo`, `created_at`, `updated_at`) VALUES
	(5, 'Categoria Noticia', '2017-12-18 21:40:24', '2017-12-18 21:40:24'),
	(6, 'Outra Categoria', '2017-12-18 21:40:30', '2017-12-18 21:40:30');
/*!40000 ALTER TABLE `noticia_categoria` ENABLE KEYS */;

-- Copiando estrutura para tabela escape.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela escape.users: 0 rows
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
