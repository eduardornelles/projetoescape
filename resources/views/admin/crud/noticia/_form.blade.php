<div class="form-group">
    {!! Form::label('titulo','Título:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('titulo', $registro->titulo , [ 'class' => 'form-control', 'required' ] ) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('noticia_categoria_id','Categoria:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        <select name="noticia_categoria_id" class="selectSingle form-control" required>
            <option value="">Selecione</option>
            @foreach($categorias as $categoria)
                <option @if($categoria->id == $registro->noticia_categoria_id) selected @endif value="{{ $categoria->id }}">{{ $categoria->titulo }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    {!! Form::label('texto','Texto:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('texto', $registro->texto , [ 'class' => 'form-control summernote' ] ) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('imagem','Imagem:', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        <input type="file" name="imagem" id="imagem" class="form-control " @if( $registro->id == null ) required @endif />
    </div>
    <div class="col-sm-10 col-md-offset-2">
        <div class="preview">
            @if( $registro->imagem != '')
                <img src="{{ asset('') }}uploads/{!! $registro->imagem !!}" style="max-width: 150px" />
            @endif
        </div>
    </div>
</div>
